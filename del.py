import requests
import re


def get_ip_address():
    """Получаем ip-адрес"""
    response = requests.get("http://checkip.dyndns.com")
    text = response.text
    # Используем регулярное выражение для поиска IP-адресов
    ip_pattern = r'\b(?:\d{1,3}\.){3}\d{1,3}\b'

    ip_addresses = re.findall(ip_pattern, text)
    return ip_addresses[0]


def get_latitude_longitude(ip_address: str):
    """Функция для получения Широты и Долготы"""
    response = requests.get(f"https://ipinfo.io/{ip_address}/json")
    data = response.json()
    # извлекаем широту и долготу из данных
    loc_value = data["loc"]
    lattitude, longitude = loc_value.split(",")
    return lattitude, longitude


if __name__ == "__main__":
    ip_address = get_ip_address()
    print(ip_address)
    get_latitude_longitude(ip_address)

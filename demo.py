import requests
from tabulate import tabulate

from data_func import get_ip_address, get_latitude_longitude

base_url = "https://api.open-meteo.com/v1/forecast"

ip_address = get_ip_address()
latitude, longitude = get_latitude_longitude(ip_address)

params = {
    "latitude": latitude,
    "longitude": longitude,
    "daily": "temperature_2m_max,temperature_2m_min",
    "timezone": "GMT",
    "forecast_days": 1
}

response = requests.get(base_url, params=params)
body = response.json()

_data = body["daily"]

all_data = [x for x in zip(_data["time"], _data["temperature_2m_max"], _data["temperature_2m_min"])]
# сформируем заголовки для столбцов таблицы
headers = ["time", "temperature_max", "temperature_min"]
# сформируем саму таблицу
table = tabulate(all_data, headers=headers, tablefmt='fancy_grid', colalign=["center"] * len(headers),
                 floatfmt=".1f", showindex=True)

if __name__ == "__main__":
    print(table)
